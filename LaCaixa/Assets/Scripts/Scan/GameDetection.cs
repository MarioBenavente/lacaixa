﻿using UnityEngine;
using System.Collections;

public class GameDetection : MonoBehaviour, Vuforia.ITrackableEventHandler
{
    private Vuforia.TrackableBehaviour mTrackableBehaviour;
    void Start()
    {
        mTrackableBehaviour = GetComponent<Vuforia.TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(
                                    Vuforia.TrackableBehaviour.Status previousStatus,
                                    Vuforia.TrackableBehaviour.Status newStatus)
    {
        if (newStatus == Vuforia.TrackableBehaviour.Status.DETECTED ||
            newStatus == Vuforia.TrackableBehaviour.Status.TRACKED)
        {
            Time.timeScale = 1;
        }else if (newStatus != Vuforia.TrackableBehaviour.Status.DETECTED &&
            newStatus != Vuforia.TrackableBehaviour.Status.TRACKED)
        {
            Time.timeScale = 0;
        }
    }
}
