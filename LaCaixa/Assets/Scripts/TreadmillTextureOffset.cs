﻿using UnityEngine;
using System.Collections;

public class TreadmillTextureOffset : MonoBehaviour {

	public float scrollSpeed = 0.5F;
	public Renderer rend;
	public Texture text;

	public bool _isTrail;

	public float _randomOffset;



	void Start () {
		rend = GetComponent<Renderer>();
		_randomOffset = Random.Range (0.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeScale > 0){
			float offset = Time.time * scrollSpeed;

			if (!_isTrail) {
				rend.materials [1].SetTextureOffset ("_MainTex", new Vector2 (0, _randomOffset + offset));
				//rend.material.SetTextureOffset(text.ToString(), new Vector2(offset, 0));
				//Debug.Log ("TEXTURE->"+rend.material.name);
			} else {
				rend.materials [0].SetTextureOffset ("_MainTex", new Vector2 (0.14f, _randomOffset + offset));
			}

		}
	}
}
