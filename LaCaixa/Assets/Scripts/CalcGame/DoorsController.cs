﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DoorsController : MonoBehaviour {
	private float mTimeToSpawnDoors=3.0f;
	private bool mFirstSpawn;
	public Transform leftDoorRespawn, centerDoorRespawn, rightDoorRespawn;
	private Door mLeftDoor, mCenterDoor, mRightDoor;
	public Door doorPrefab;
	private List<Operation> mOperationList;
	private Operation mCurrentOperation;
	private bool playerHasCollidedInThisWave;

	// Use this for initialization
	void Start () {

		mOperationList = new List<Operation>();
		mOperationList.Add(new Operation(1,"2+5", "7", new string[] { "7", "6", "8" },"easy"));
		mOperationList.Add(new Operation(1,"4+6", "10", new string[] { "9", "10", "11" },"easy"));
		mOperationList.Add(new Operation(1,"8+6", "14", new string[] { "14", "12", "13" },"easy"));
		mOperationList.Add(new Operation(1,"8+7", "15", new string[] { "15", "16", "17" },"easy"));
		mOperationList.Add(new Operation(1,"14-3", "11", new string[] { "9", "10", "11" },"easy"));
		mOperationList.Add(new Operation(1,"13-7", "6", new string[] { "7", "8", "6" },"easy"));
		mOperationList.Add(new Operation(1,"16-9", "7", new string[] { "7", "6", "8" },"easy"));
		mOperationList.Add(new Operation(1,"11+9", "20", new string[] { "21", "20", "22" },"easy"));



		mOperationList.Add(new Operation(1,"27:3", "9", new string[] { "9", "10", "11" },"normal"));
		mOperationList.Add(new Operation(1,"2*6", "12", new string[] { "16", "12", "14" },"normal"));
		mOperationList.Add(new Operation(1,"4*6", "24", new string[] { "24", "28", "32" },"normal"));
		mOperationList.Add(new Operation(1,"18:9", "2", new string[] { "1", "2", "0" },"normal"));
		mOperationList.Add(new Operation(1,"7*8", "56", new string[] { "42", "49", "56" },"normal"));
		mOperationList.Add(new Operation(1,"9*7", "63", new string[] { "54", "63", "72" },"normal"));
		mOperationList.Add(new Operation(1,"24:8", "3", new string[] { "3", "4", "5" },"normal"));
		mOperationList.Add(new Operation(1,"42:6", "7", new string[] { "6", "7", "8" },"normal"));


		mOperationList.Add(new Operation(1,"4*3+5", "17", new string[] { "15", "16", "17" },"hard"));
		mOperationList.Add(new Operation(1,"6*4-4", "20", new string[] { "18", "19", "20" },"hard"));
		mOperationList.Add(new Operation(1,"8*8+8", "72", new string[] { "72", "80", "88" },"hard"));
		mOperationList.Add(new Operation(1,"8*7-6", "50", new string[] { "48", "49", "50" },"hard"));
		mOperationList.Add(new Operation(1,"5:1-4", "1", new string[] { "0", "1", "2" },"hard"));
		mOperationList.Add(new Operation(1,"16:4+4", "8", new string[] { "6", "7", "8" },"hard"));
		mOperationList.Add(new Operation(1,"63:7+11", "20", new string[] { "18", "19", "20" },"hard"));
		mOperationList.Add(new Operation(1,"54:9+7", "13", new string[] { "13", "14", "15" },"hard"));

		Init();
	}

	public void Init()
	{
		Invoke("SpawnDoors", mTimeToSpawnDoors);
	}

	public void FixedUpdate()
	{
		CheckDoorsCollided();
		CheckDoorsAreDestroyed();
	}



	// Update is called once per frame
	void Update () {

	}

	public void SpawnDoors()
	{
		mCurrentOperation = GetRandomOperation(CalcGameController.instance.currentDifficulty);

		CalcGameController.instance.calcUIController.operationText.text = mCurrentOperation.operation;

		mLeftDoor = (Door) Instantiate(doorPrefab, leftDoorRespawn.transform.position, doorPrefab.transform.rotation);
		mLeftDoor.textMesh.text = mCurrentOperation.suggestionSolutions[0];
		mLeftDoor.transform.parent = gameObject.transform;

		mCenterDoor = (Door)Instantiate(doorPrefab, centerDoorRespawn.transform.position, doorPrefab.transform.rotation);
		mCenterDoor.textMesh.text = mCurrentOperation.suggestionSolutions[1];
		mCenterDoor.transform.parent = gameObject.transform;

		mRightDoor = (Door) Instantiate(doorPrefab, rightDoorRespawn.transform.position, doorPrefab.transform.rotation);
		mRightDoor.textMesh.text = mCurrentOperation.suggestionSolutions[2];
		mRightDoor.transform.parent = gameObject.transform;

		mFirstSpawn = true;
		playerHasCollidedInThisWave = false;
	}

	private Operation GetRandomOperation( string _difficulty)
	{

		List<Operation> tempOperationList = new List<Operation> ();

		//select the operations with the desired diffculty
		for (int i = 0; i < mOperationList.Count - 1; i++) {

			if (mOperationList [i].diffciulty == _difficulty) {
				tempOperationList.Add (mOperationList [i]);
			}
		}

		int position = (int)Random.Range(0f, tempOperationList.Count - 1);

		tempOperationList[position].RandomizeSuggestionSolution();
		return tempOperationList[position];
	}

	private void CheckDoorsAreDestroyed()
	{
		if (mFirstSpawn && mLeftDoor==null && mCenterDoor == null && mRightDoor == null)
		{
			SpawnDoors();
		}
	}

	private void CheckDoorsCollided()
	{
		if(mLeftDoor != null && mCenterDoor != null && mRightDoor != null && !playerHasCollidedInThisWave)
		{
			if (mLeftDoor.isCollided)
			{
				if (mLeftDoor.textMesh.text.Equals(mCurrentOperation.solution))
				{
					mLeftDoor.ChangeMaterial ("Correct");
					CalcGameController.instance.SetScore(CalcGameController.instance.score+1);
				}
				else
				{
					mLeftDoor.ChangeMaterial ("Error");
					//CalcGameController.instance.SetScore(CalcGameController.instance.score - 1);
				}
				playerHasCollidedInThisWave = true;
			}
			else if (mCenterDoor.isCollided)
			{
				if (mCenterDoor.textMesh.text.Equals(mCurrentOperation.solution))
				{
					mCenterDoor.ChangeMaterial ("Correct");
					CalcGameController.instance.SetScore(CalcGameController.instance.score + 1);

				}
				else
				{
					mCenterDoor.ChangeMaterial ("Error");
					//CalcGameController.instance.SetScore(CalcGameController.instance.score - 1);

				}
				playerHasCollidedInThisWave = true;
			}
			else if (mRightDoor.isCollided)
			{
				if (mRightDoor.textMesh.text.Equals(mCurrentOperation.solution))
				{
					mRightDoor.ChangeMaterial ("Correct");
					CalcGameController.instance.SetScore(CalcGameController.instance.score + 1);
				}
				else
				{
					mRightDoor.ChangeMaterial ("Error");
					//CalcGameController.instance.SetScore(CalcGameController.instance.score - 1);
				}
				playerHasCollidedInThisWave = true;
			}
		}
	}

}