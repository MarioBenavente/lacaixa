﻿using UnityEngine;
using System.Collections;

public class CalcGameController : MonoBehaviour {
    public static CalcGameController instance;
    public PlayerCalcController mPlayerController;
    public CalcSwipeDetector swipeDetector;
    DoorsController mDoorController;
    private int mScore;
    public float gameSpeed = 1;
    public int score
    {
        get { return mScore; }
    }


    public DoorsController doorController
    {
        get { return mDoorController; }
    }
    public CalcUIController calcUIController;


	// Game time

	public float mMaxPlayTime = 90;
	public float mCurrentTime;

	private string mCurrentDifficulty;

	public string currentDifficulty{
		get{return mCurrentDifficulty; }
	}


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Time.timeScale = 0;
        }
        else
        {
            Destroy(this);
        }
    }

    // Use this for initialization
    void Start () {

		mMaxPlayTime = Time.deltaTime + mMaxPlayTime;


        Physics.gravity = new Vector3(0, -600f, 0);
        mPlayerController = transform.Find("PlayerCalcController").GetComponent<PlayerCalcController>();
        mDoorController = transform.Find("DoorsController").GetComponent<DoorsController>();
    }

    void FixedUpdate()
    {
		TimeCounter ();
		ManageScore ();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetScore(int score)
    {
        mScore = score;
		calcUIController.IncreaseScore ();
    }

	public void TimeCounter(){


		mCurrentTime += Time.deltaTime;

		calcUIController.SetCronometer ((int)mMaxPlayTime - (int)mCurrentTime);

		if (mCurrentTime >= mMaxPlayTime) {

		
		}


	}

	public void ManageScore(){

		if (mScore >= 0 && mScore <= 4) {

			mCurrentDifficulty = "easy";

		} else if (mScore >= 5 && mScore <= 10) {

			mCurrentDifficulty = "normal";

		} else if (mScore >= 11 && mScore <= 16) {
		
			mCurrentDifficulty = "hard";
		}

	}


}
