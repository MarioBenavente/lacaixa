﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
    private float mSpeed=0.5f;

    public TextMesh textMesh;
    public bool isCollided;


	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GetComponent<Rigidbody>().position = new Vector3(GetComponent<Rigidbody>().position.x, GetComponent<Rigidbody>().position.y, GetComponent<Rigidbody>().position.z+mSpeed*CalcGameController.instance.gameSpeed);
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isCollided = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            CalcGameController.instance.swipeDetector.ResetDoubleTouch();
        }
    }

	public void ChangeMaterial (string name){
		switch (name) {
		case "Error":
			GetComponent<Renderer> ().materials [1].SetColor ("_Color", new Color(1.0f, 0.1f, 0.0f, 0.0f));
			GetComponent<Renderer> ().materials [1].SetColor ("_EmissionColor", new Color(1.0f, 0.1f, 0.0f, 0.0f));
			break;
		case "Correct":
			GetComponent<Renderer> ().materials [1].SetColor ("_Color", new Color(0.2f, 1.0f, 0.2f, 0.0f));
			GetComponent<Renderer> ().materials [1].SetColor ("_EmissionColor",new Color(0.2f, 1.0f, 0.2f, 0.0f));
			break;
		}
	}
}
