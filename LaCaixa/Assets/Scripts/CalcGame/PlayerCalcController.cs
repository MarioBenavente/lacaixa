﻿using UnityEngine;
using System.Collections;

public class PlayerCalcController : MonoBehaviour {

    public enum PlayerPositionEnum
    {
        LEFT, CENTER, RIGHT
    }

    private float mMoveForce = 4300.0f;
    private PlayerPositionEnum playerPositionEnum;
    public bool isFloor = true;

    private Vector3 leftCorrectionDiff, centerCorrectionDiff, rightCorrectionDiff;
    private bool bLeftCorrectionDiff, bCenterCorrectionDiff, bRightCorrectionDiff;

    // Use this for initialization
    void Start () {

        playerPositionEnum = PlayerPositionEnum.CENTER;
       if (!bCenterCorrectionDiff)
        {
            centerCorrectionDiff = GetComponent<Rigidbody>().position - CalcGameController.instance.transform.position;
            bCenterCorrectionDiff = true;
        }


    }
	
    void FixedUpdate()
    {
        Move();
    }


	// Update is called once per frame
	void Update () {
	
	}

    void Move()
    {
        if (isFloor)
        {
            if (CalcGameController.instance.swipeDetector.doubleTouch)
            {
                CalcGameController.instance.gameSpeed = 2;
				ChangeColorDoubleTap ();
            }
            else
            {
				
                CalcGameController.instance.gameSpeed = 1;
                // Para swipe
                switch (CalcGameController.instance.swipeDetector.swipeDirection)
                {
                    case CalcSwipeDetector.SwipeDirection.LEFT:
                        if (playerPositionEnum != PlayerPositionEnum.LEFT)
                        {
                            GetComponent<Rigidbody>().AddForce((Vector3.up + Vector3.right).normalized * mMoveForce);
                            isFloor = false;
                            playerPositionEnum = playerPositionEnum - 1;
                        }
                        CalcGameController.instance.swipeDetector.swipeDirection = CalcSwipeDetector.SwipeDirection.NONE;
                        break;
                    case CalcSwipeDetector.SwipeDirection.RIGHT:
                        if (playerPositionEnum != PlayerPositionEnum.RIGHT)
                        {
                            GetComponent<Rigidbody>().AddForce((Vector3.up + Vector3.left).normalized * mMoveForce);
                            isFloor = false;
                            playerPositionEnum = playerPositionEnum + 1;
                        }
                        CalcGameController.instance.swipeDetector.swipeDirection = CalcSwipeDetector.SwipeDirection.NONE;
                        break;
                    default:

                        break;

                }
            }
        }else
        {
            CalcGameController.instance.gameSpeed = 1;
            CalcGameController.instance.swipeDetector.swipeDirection = CalcSwipeDetector.SwipeDirection.NONE;
        }
                        
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "FloorCalc")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
            isFloor = true;
         //   GetComponent<Rigidbody>().isKinematic = true;
            if (playerPositionEnum == PlayerPositionEnum.LEFT)
            {
                if (!bLeftCorrectionDiff)
                {
                    leftCorrectionDiff =  GetComponent<Rigidbody>().position - CalcGameController.instance.transform.position;
                    bLeftCorrectionDiff = true;
                }
                else
                {
                    GetComponent<Rigidbody>().position = leftCorrectionDiff + CalcGameController.instance.transform.position;
                }
            }else if (playerPositionEnum == PlayerPositionEnum.RIGHT) {
                if (!bRightCorrectionDiff)
                {
                    rightCorrectionDiff =  GetComponent<Rigidbody>().position - CalcGameController.instance.transform.position;
                    bRightCorrectionDiff = true;
                }
                else
                {
                    GetComponent<Rigidbody>().position = rightCorrectionDiff + CalcGameController.instance.transform.position;
                }
            }
            else if (playerPositionEnum == PlayerPositionEnum.CENTER)
            {
                if (!bCenterCorrectionDiff)
                {
                    centerCorrectionDiff =   GetComponent<Rigidbody>().position - CalcGameController.instance.transform.position;
                    bCenterCorrectionDiff = true;
                }
                else
                {
                    GetComponent<Rigidbody>().position = centerCorrectionDiff +CalcGameController.instance.transform.position;
                }
            }
        }
    }


	private void ChangeColorDoubleTap(){
		//change color material
		GetComponent<Renderer> ().materials [0].SetColor ("_Color", new Color(1.0f, 0.3f, 0.0f, 0.0f));
		GetComponent<Renderer> ().materials [0].SetColor ("_EmissionColor", new Color(1.0f, 0.3f, 0.0f, 0.0f));

		//change color parycle system
		this.GetComponentsInChildren<ParticleSystem> ()[0].startColor = new Color (1.0f, 0.3f, 0.0f, 0.5f);
	}

	public void ChangeColorNormal(){
		//change color material
		GetComponent<Renderer> ().materials [0].SetColor ("_Color", new Color(0.6078f, 0.874509f, 0.9725f, 0.0f));
		GetComponent<Renderer> ().materials [0].SetColor ("_EmissionColor", new Color(0.0f, 0.733f, 1.0f, 0.0f));

		//change color parycle system
		this.GetComponentsInChildren<ParticleSystem> ()[0].startColor = new Color (0.212f, 0.698f, 0.87745f, 0.5f);
	}

}
