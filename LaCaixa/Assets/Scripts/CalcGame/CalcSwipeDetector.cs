﻿using UnityEngine;
using System.Collections;

public class CalcSwipeDetector : MonoBehaviour {
    private float _minSwipeDistY;
    private float _minSwipeDistX;
    private Vector2 _oldPos;
    public SwipeDirection swipeDirection = SwipeDirection.NONE;
    private bool mFirstTouchForDoubleTouch;
    public bool doubleTouch;
	public GameObject speedPS;
	public Transform speedPsSpawnPos;

    public enum SwipeDirection
    {
        NONE, LEFT, RIGHT
    }

    void Start()
    {
        _minSwipeDistY = Screen.width * 0.005f;
        _minSwipeDistX = Screen.width * 0.005f;
        Invoke("CheckResetDoubleTouch", 0.1f);




    }

    void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _oldPos = new Vector2(touch.position.x, touch.position.y);
                    break;
                case TouchPhase.Stationary:
                    _oldPos = new Vector2(touch.position.x, touch.position.y);
                    break;
                case TouchPhase.Ended:
                    Swipe(touch);
                    DoubleTouch(touch);
                    break;
                case TouchPhase.Moved:
                    Swipe(touch);
                    break;
            }
        }
    }

    void Swipe(Touch touch)
    {
        if (!doubleTouch)
        {
            float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, _oldPos.y, 0)).magnitude;
            float swipeDistHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(_oldPos.x, 0, 0)).magnitude;
            if (swipeDistHorizontal >= _minSwipeDistX)
            {
                float swipeValue = Mathf.Sign(touch.position.x - _oldPos.x);
                if (swipeValue > 0) swipeDirection = SwipeDirection.RIGHT;
                else if (swipeValue < 0) swipeDirection = SwipeDirection.LEFT;
                mFirstTouchForDoubleTouch = false;
            }
            else
            {
                mFirstTouchForDoubleTouch = true;
            }
        }
        _oldPos = new Vector2(Input.touches[0].position.x, Input.touches[0].position.y);
    }

    void DoubleTouch(Touch touch)
    {



        float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, _oldPos.y, 0)).magnitude;
        float swipeDistHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(_oldPos.x, 0, 0)).magnitude;
        if (swipeDistHorizontal < _minSwipeDistX && swipeDirection == SwipeDirection.NONE && CalcGameController.instance.mPlayerController.isFloor)
        {
            if (mFirstTouchForDoubleTouch)
            {
                doubleTouch = true;
				Instantiate (speedPS, speedPsSpawnPos.position, Quaternion.identity);
            }
            else
            {
                mFirstTouchForDoubleTouch = true;
				Instantiate (speedPS, speedPsSpawnPos.position, Quaternion.identity);

            }
        }else
        {
			
            ResetDoubleTouch();
        }

    }

    void CheckResetDoubleTouch()
    {
        if (!doubleTouch)
        {
            ResetDoubleTouch();
        }
        Invoke("CheckResetDoubleTouch", 0.1f);
    }

    public void ResetDoubleTouch()
    {
        doubleTouch = false;
        mFirstTouchForDoubleTouch = false;
		//Delete speedPS
		Destroy(GameObject.FindGameObjectWithTag("SpeedPS"));
		GameObject.Find ("PlayerCalcController").GetComponent<PlayerCalcController> ().ChangeColorNormal ();
    }
}
