﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CalcUIController : MonoBehaviour {

    public Text operationText;
   	
	public GameObject[] _scoreList;

	public Sprite _point;

	private int _currentUIScore = 0;

	public Text cronometer;

	// Use this for initialization
	void Start () {
        
    }

	public void IncreaseScore(  ){


		_scoreList [_currentUIScore].GetComponent<Image> ().sprite = _point;
		_currentUIScore++;

	}

	public void SetCronometer(int time){

		cronometer.text = time.ToString () + '"';
	}


}
