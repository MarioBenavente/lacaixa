﻿using UnityEngine;
using System.Collections;

public class Operation{

    string mOperation;
    public string operation
    {
        get { return mOperation; }
    }

    string mSolution;
    public string solution
    {
        get { return mSolution; }
    }

    string []mSuggestionSolutions;
    public string[] suggestionSolutions
    {
        get { return mSuggestionSolutions; }
    }

	int mId;
	public int id{

		get{ return mId;}
	}

	string mDifficulty;
	public string diffciulty{
		get{ return mDifficulty;}
	}



	public Operation(int id, string operation, string solution,string [] suggestionSolutions , string difficulty)
    {
		mId = id;
        mOperation = operation;
        mSolution = solution;
        mSuggestionSolutions = suggestionSolutions;
		mDifficulty = difficulty;
    }

    public void RandomizeSuggestionSolution()
    {
        for (int i = 0; i < mSuggestionSolutions.Length; i++)
        {
           int position = (int)Random.Range(0f, mSuggestionSolutions.Length - 1);
           string copy = mSuggestionSolutions[i];
           mSuggestionSolutions[i] = mSuggestionSolutions[position];
           mSuggestionSolutions[position] = copy;
        }
    }

}
