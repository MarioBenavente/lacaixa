﻿using UnityEngine;
using System.Collections;

public class BoxDestroyer : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Door>() != null)
        {
            Destroy(other.gameObject);
        }
    }

}
